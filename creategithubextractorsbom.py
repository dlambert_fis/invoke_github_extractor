#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from pathlib import Path
from shutil import which
from urllib import request
from urllib.parse import urljoin

import sys,json,subprocess,time,shutil
import pprint; pp=pprint.PrettyPrinter(indent=4)

CREATE_SBOM = Path(r'./bin/create_sbom.sh').resolve()
GITHUB_EXTRACTOR = Path(r'./bin/github_extractor').resolve()
GITHUB_LINGUIST = 'github-linguist'
CYCLONEDX_CLI = 'cyclonedx-cli'
SBOM_CONVERTER = Path(r'./bin/sbom_converter').resolve()


class CreateGithubExtractorSBOM:
    r'''
        Run github_linguist binaries using bash helper script named create_sbom.sh.

        Requirements:
            The required binaries in environment path are:
                github-linguist https://github.com/github/linguist/
                cyclonedx-cli https://github.com/CycloneDX/cyclonedx-cli
                github_extractor
                sbom_converter
        
        Dependencies:
            git, zip, grep, create_sbom.sh

        Arguments:
            owner: <str>
                Default: 'github'
                The owner of the project used to build the Github link
                    https://github.com/<owner>/<project>
            
            path_to_config: <str> 
                Default: './bin/config.json'
                The path to config.json file used by github_extractor,
                requires github.token.ACCESS_TOKEN to be modified.
                "github": {
	                "token": "ACCESS_TOKEN",...

            project: <str>
                Default: 'linguist'
                The project name used to build the Github link
                https://github.com/<owner>/<project>


            working_directory: <str>
                Default: '.'
                All output and repository data is written in a unique sub-directory 
                (see attribute time_folder: naming convention 9999999_99999) under the working_directory.

        Attributes:
            completed_process: <CompletedProcess>
                The resulting subprocess.CompletedProcess class returned by run_create_sbom().

            sbom_directory: <Path>
                Default: working_directory/time_folder/sboms
                The directory that function move_sboms will copy SBoMs to.

            time_folder: <str>
                A unique folder with naming convention 9999999_99999 to store all data in.

        Examples:
            # import
            from creategithubextractorsbom import CreateGithubExtractorSBOM
            
            # get help
            help(CreateGithubExtractorSBOM)
            
            # Recommended process (order matters to prevent data loss)
            from creategithubextractorsbom import CreateGithubExtractorSBOM
            cges = CreateGithubExtractorSBOM(path_to_config = <CONFIG>,
                                        working_directory = <WORKING_DIRECTORY>,
                                        owner = <owner>,
                                        project = <project>)    #1
            cges.sbom_directory = Path(<SBOMS_DIRECTORY>)   #2
            a_url = cges.build_github_url()             #3
            sbom_status = cges.run_create_sbom()        #4
            cges.copy_sboms()                           #5 copies to cges.sbom_directory
            cges.clean_up(keep_zips=True)               #6


            
'''

    def __init__(self,owner: str='github',
                    project: str='linguist',
                    path_to_config: str=r'./bin/config.json', 
                    working_directory: str=None

                ) -> None:
        
        self.owner = owner
        self.project = project
        self.path_to_config = Path(path_to_config).resolve()
        self._path_to_create_sbom = Path(r'./bin/create_sbom.sh').resolve()
        self._github_base_url = r'https://github.com/'
        
        self.time_folder = str(time.time()).replace('.','_')
        self.working_directory = working_directory
        self.sbom_directory = None
        self.completed_process = None

        return None

    def check_dependencies(self):
        '''Check dependencies, return 0 if all dependencies OK, otherwise return -1.
        
        '''
        fin = 0
        check_github_linguist = which(GITHUB_LINGUIST)
        check_create_sbom = which(str(CREATE_SBOM))
        check_github_extractor = which(GITHUB_EXTRACTOR)
        check_sbom_converter = which(str(SBOM_CONVERTER))
        check_cyclonedx_cli = which(CYCLONEDX_CLI)
        check_git = which('git')
        check_zip = which('zip')
        check_config_exists = self.path_to_config.exists()
        config_data = None

        name = 'InvokeGithubExtractor.check_dependencies() '
        if check_github_linguist == None:
            print(name,'github-linguist binary not found',file=sys.stderr,flush=True)
            fin = -1
        
        if check_create_sbom == None:
            print(name,'create_sbom.sh not found',file=sys.stderr,flush=True)
            fin = -1

        if check_github_extractor == None:
            print(name,'github_extractor not found',file=sys.stderr,flush=True)
            fin = -1

        if check_sbom_converter == None:
            print(name,'sbom_converter not found',file=sys.stderr,flush=True)
            fin = -1
        
        if check_cyclonedx_cli == None:
            print(name,'cyclonedx_cli not found',file=sys.stderr,flush=True)
            fin = -1
        
        if check_git == None:
            print(name,'git not found',file=sys.stderr,flush=True)
            fin = -1
        
        if check_zip == None:
            print(name,'zip not found',file=sys.stderr,flush=True)
            fin = -1
        
        #todo check config, check for api token
        if check_config_exists == False:
            print(name,r'config.json not found, should be in ./bin/',file=sys.stderr,flush=True)
            fin = -1
        
        if check_config_exists:
            with open(self.config) as fp:
                config_data = json.load(fp)
            if len(config_data['github']['token']) == 0:
                print(name,r'config.json requires github.token',file=sys.stderr,flush=True)
                fin = -1
        
        return fin
    
    def _set_dir(self,base_dir: Path=Path('.'),child_dir: str='',exist_ok=False):
        
        a_base = base_dir.resolve()
        fin = a_base.joinpath(child_dir)
        fin.mkdir(exist_ok=exist_ok)
        return fin.resolve()

    def build_github_url(self,owner: str=None, project: str=None, base: str=None):
        if owner == None:
            owner = self.owner
        if project == None:
            project = self.project
        if base == None:
            base = self._github_base_url
        url = urljoin(base,owner+'/'+project+'.git')
        
        return url

    def check_url_exists(self,url: str):
        try:
            http_code = request.urlopen(url).getcode()
            exists = http_code > 199 and http_code < 300
        except:
            return False
        
        return exists

    def run_create_sbom(self, owner: str=None,
                            project: str=None,
                            working_directory: Path=None,
                            path_to_create_sbom: Path=None,
                            path_to_config: Path=None
                        ):
        r'''
        Setup args and run create_sbom.sh for single project -> CompletedProcess
        
        '''
        name = 'CreateGithubExtractorSBOM.run_create_sbom()|'
        fail = False
        if owner == None:
            owner = self.owner
        if project == None:
            project = self.project
        if working_directory == None:
            working_directory = Path(self.working_directory).resolve()
        if path_to_create_sbom == None:
            path_to_create_sbom = Path(self._path_to_create_sbom).resolve()
        if path_to_config == None:
            path_to_config = Path(self.path_to_config).resolve()
        if working_directory == None:
            working_directory = self._set_dir(base_dir=Path('.'),child_dir=self.time_folder)
            # print(name,'working_directory is None',file=sys.stderr,flush=True)
            # fail = True
        if path_to_create_sbom == None:
            print(name,'path to create_sbom.sh is None',file=sys.stderr,flush=True)
            fail = True
        if path_to_config == None:
            print(name,'path to config.json is None',file=sys.stderr,flush=True)
            fail = True
        if fail:
            return -1
        
        self.working_directory = working_directory
        flags = [str(path_to_create_sbom),'-d',str(working_directory),'-c',str(path_to_config),'-o',owner,'-p',project]
        
        
        self.completed_process = subprocess.run(flags,capture_output=True,text=True)

        return self.completed_process

    def copy_sboms(self,to_path: Path=None):
        r'''copy_sboms() -> list of original files compied.
            Call before clean_up() to prevent lossing SBoMs.
            Copy SBoMs (files named *_sbom.*) from working_directory to to_path, 
            creates to_path directory if not exists,
            clobbers existing SBoMs with same file names.
            Copy Fail SBoMs in fail folder
        '''
        
        
        if to_path == None:
            to_path = self.sbom_directory
        if to_path == None:
            to_path = self._set_dir(base_dir=self.working_directory,child_dir='sboms',exist_ok=True)
            self.sbom_directory = to_path
        
        to_path.mkdir(parents=True, exist_ok=True)
        to_path.resolve()
        
        sbom_dir = self.owner+'_'+self.project+'_output'
        sbom_path = self.working_directory.joinpath(sbom_dir).resolve()
        sbom_list = sorted(sbom_path.glob('*sbom*'))

        for a_file in sbom_list:
            shutil.copy(src=a_file,dst=to_path)

        return sbom_list

    def clean_up(self,keep_zips: bool=False):
        r'''Clean up after run_create_sbom() and copy_sboms()
            Call copy_sboms() first, to prevent loss of SBoMs
            If keep_zips True only delete repository and output directories.
            If keep_zips False delete working_directory.
        '''
        
        if keep_zips:
            repo_path = self.working_directory.joinpath(self.project).resolve()
            output_path = self.working_directory.joinpath(self.owner+'_'+self.project+'_output').resolve()
            shutil.rmtree(repo_path)
            shutil.rmtree(output_path)
            return 0
        
        shutil.rmtree(self.working_directory)
        return 0

