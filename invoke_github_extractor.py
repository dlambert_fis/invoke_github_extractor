#!/usr/bin/env python
# -*- encoding: utf-8 -*-


from pathlib import Path
import os,sys,csv,argparse,json,time
from creategithubextractorsbom import CreateGithubExtractorSBOM
from multiprocessing import cpu_count,Process
from datetime import date, datetime
from time import sleep
import pprint; pp=pprint.PrettyPrinter(indent=4)

# ---- GLOBAL SETTINGS ------------
CSV_PATH = r'sample_owner_project.csv'
CONFIG = r'./config/config.json'
WORKING_DIRECTORY = r'./data/'
SBOMS_DIRECTORY = r'./data/sboms'
KEEP_ZIPS = False
KEEP_DATA = False
NUM_PROCS = 2
VERSION = '0.2.0'
WAIT = 30
MIN_NUM_SBOMS = 2 # XML JSON
MAX_CYCLES = NUM_PROCS * 8

def create_sbom(working_directory, owner: str='ja-netfilter',project: str='ja-netfilter'):
    global CONFIG, SBOMS_DIRECTORY, KEEP_ZIPS, KEEP_DATA, MIN_NUM_SBOMS

    cges = CreateGithubExtractorSBOM(path_to_config = CONFIG,
                                        owner = owner,
                                        project = project) #1
    cges.sbom_directory = Path(SBOMS_DIRECTORY).resolve() #2
    a_url = cges.build_github_url()#3
    
    if cges.check_url_exists(a_url) == False:
        print(owner,project,'repository not exist',file=sys.stderr,flush=True)
        return -1
    
    #create sub directory as we expect several calls to create_sbom() under a single process dir
    time_folder =  str(time.time()).replace('.','_')
    sub_working_directory = Path(working_directory).joinpath(time_folder).resolve()
    sub_working_directory.mkdir(parents=True) #should not exist
    
    print('Run github_extractor for repository:',owner,project,'working directory:',sub_working_directory,flush=True)
    sbom_status = cges.run_create_sbom(working_directory=sub_working_directory)#4
    
    if sbom_status.returncode != 0:
        print(owner,project,'Error github_extractor or create_sbom.sh',file=sys.stderr,flush=True)
        print('Check',owner+'_'+project+r'_output/github_extractor.log, errors below (mv: cannot stat, are common).',file=sys.stderr,flush=True)
        print(str(sbom_status.stderr),file=sys.stderr,flush=True)
        return -1
    
    print(owner,project,'done, return code:',str(sbom_status.returncode),flush=True)
    print(owner,project,'Copying SBoMs to:',str(SBOMS_DIRECTORY),flush=True)
    cges.copy_sboms()#5
    
    # should have at least minimum number of sboms or more
    search_str = owner+'_'+project+'*sbom*'
    num_sboms = len( list( cges.sbom_directory.glob(search_str) ) )
    if num_sboms < MIN_NUM_SBOMS:
        print(owner,project,'SBoMs Fail')
    
    if KEEP_DATA == False:
        print(owner,project,'Cleaning up')
        cges.clean_up(keep_zips=KEEP_ZIPS)#6
    print(owner,project,'Fin')
    return cges.completed_process

def set_args():
    d = 'Generate SBoMs from CSV file with columns owner,project.'
    global CONFIG, WORKING_DIRECTORY, SBOMS_DIRECTORY, KEEP_ZIPS, KEEP_DATA, NUM_PROCS, CSV_PATH,VERSION,MAX_CYCLES
    parser = argparse.ArgumentParser(description=d)
    parser.add_argument('--input-csv','-i',
                        dest='csv_file',
                        help='The path to CSV file containing only columns owner,project in order.',
                        required=False,
                        default=CSV_PATH
                        )
    parser.add_argument('--keep-zips','-k',
                        dest='keep_zips',
                        action='store_true',
                        help='Keep repository and github_extractor output as zip files.',
                        required=False,
                        default=KEEP_ZIPS
                        )
    parser.add_argument('--num-procs','-n',
                        dest='num_procs',
                        help='The number of process for multiprocessing, should be less than number of cores for the system. More processes for a single repository has no affect.',
                        required=False,
                        type=int,
                        default=NUM_PROCS
                        )
    parser.add_argument('--config','-c',
                        dest='config',
                        help='Path to config.json file used by github_extractor.',
                        required=False,
                        default=CONFIG
                        )
    parser.add_argument('--working-dir','-w',
                        dest='working_dir',
                        help='The working where unique folders created for each process and sub-folders which hold repository and github_extractor output',
                        required=False,
                        default=WORKING_DIRECTORY
                        )
    parser.add_argument('--sboms-dir','-s',
                        dest='sboms_dir',
                        help='All SBoMs will be copied to this directory.',
                        required=False,
                        default=SBOMS_DIRECTORY
                        )
    parser.add_argument('--keep-data',
                        dest='keep_data',
                        action='store_true',
                        help='Skip clean up to keep all repository and output data (ignores --keep-zips).',
                        default=KEEP_DATA,
                        required=False
                        )
    parser.add_argument('--version',
                        action='version', 
                        version='%(prog)s '+VERSION
                        )
    some_args = parser.parse_args()
    CSV_PATH = Path(some_args.csv_file)
    KEEP_ZIPS = some_args.keep_zips
    NUM_PROCS = some_args.num_procs
    CONFIG = some_args.config
    SBOMS_DIRECTORY = some_args.sboms_dir
    KEEP_DATA = some_args.keep_data
    MAX_CYCLES = NUM_PROCS * 10
    WORKING_DIRECTORY = some_args.working_dir
    Path(WORKING_DIRECTORY).resolve().mkdir(parents=True, exist_ok=True)

    return some_args

def check_procs():
    r'''Check if num_procs is less than number of system cores.
    '''
    global NUM_PROCS
    if NUM_PROCS < 1:
        NUM_PROCS = 1

    if NUM_PROCS > cpu_count():
        print('WARNING: Number of processes should be less than number of CPU cores.')
        print('To avoid warning use flag -n ',str(cpu_count()))
        print('You have 10 seconds to kill me.')
        sleep(10)
    
    return 0

def get_owner_project_lists(path_csv: Path):
    r'''Read SCV file -> [[owner,project],...]
    '''
    owner_project_list = []
    full_path = path_csv.resolve(strict=True)
    with full_path.open() as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            owner = row['owner']
            project = row['project']
            owner_project_list.append([owner,project])
            

    return owner_project_list

def build_procs_dict(owner_projects: list,num_procs: int):
    global SBOMS_DIRECTORY
    procs_dict = {}
        
    # build empty procs_dict
    for procs_id in range(0,num_procs):
        procs_dict.update({procs_id:{'owner_projects':[],
                                    'project_count':None,
                                    'process':None,
                                    'start_time':None,
                                    'end_time':None,
                                    'duration_sec':None,
                                    'sbom_directory':SBOMS_DIRECTORY,
                                    'working_directory':None
                                    }
                        })
    # fill procs_dict with owner projects
    for idx,a_op in enumerate(owner_projects):
        procs_id = idx % num_procs
        procs_dict[procs_id]['owner_projects'].append(a_op)
    
    # fill procs_dict with project_count
    for procs_id,a_dict in procs_dict.items():
        a_dict['project_count'] = len(a_dict['owner_projects'])
    
    return procs_dict

def do_create_sboms(working_directory,owner_projects: list):
    completed_procs = []
    completed_proc = None
    for a_op in owner_projects:
        completed_proc = create_sbom(owner=a_op[0],
                                            project=a_op[1],
                                            working_directory=working_directory
                                            )
        if completed_proc == -1:
            continue

        completed_procs.append(completed_proc)
    
    return completed_procs

def do_multiprocess(procs_dict: dict):
    global WORKING_DIRECTORY, WAIT
    
    # start process
    for proc_id,p_dict in procs_dict.items():
        #create dir for a single process, create_sbom() will handle sub dirs
        time_folder =  str(time.time()).replace('.','_')
        working_directory = Path(WORKING_DIRECTORY).joinpath(time_folder).resolve()
        working_directory.mkdir(exist_ok=True)
        p_dict['working_directory'] = str(working_directory)
        
        name = 'CreateGithubExtractorSBOM'
        owner_projects = p_dict['owner_projects']
        p_dict['process'] = Process(
                                    target=do_create_sboms,
                                    kwargs={'owner_projects':owner_projects,'working_directory':working_directory},
                                    name=name
                                    )
        
        now = datetime.now().isoformat()
        p_dict['start_time'] = now
        p_dict['process'].start()
        sleep(5)


    # join process to wait
    for proc_id,p_dict in procs_dict.items():
        p_dict['process'].join()

    # process done record
    for proc_id,p_dict in procs_dict.items():
        now = datetime.now()
        start = datetime.fromisoformat(p_dict['start_time'])
        duration = now - start
        p_dict['end_time'] = now.isoformat()
        p_dict['duration_sec'] = duration.total_seconds()

    return procs_dict

def close_procs(procs_dict: dict):
    for proc_id,p_dict in procs_dict.items():
        rt = p_dict['process'].exitcode
        p_dict['process'].close()
        p_dict['process'] = rt
    return 0
    
def dump_procs_dict(procs_dict: dict):
    global WORKING_DIRECTORY

    dump_dir = Path(WORKING_DIRECTORY).resolve()
    time_stamp = datetime.now().strftime(r'%Y%m%dT%H%M%S')
    file_name = 'invoke_github_extractor_results_'+time_stamp+'.json'
    dump_file = dump_dir.joinpath(file_name)

    with dump_file.open(mode='w') as df:
        json.dump(procs_dict,df,indent=2)
    
    return 0

def main():
    global KEEP_ZIPS,NUM_PROCS,CSV_PATH,MAX_CYCLES,WAIT

    args = set_args() #sets globals
    
    check_procs() # annoying wait if greater than cpu count

    owner_projects = get_owner_project_lists(path_csv=CSV_PATH) # list of lists

    op_len = len(owner_projects) + 1
    start = 0
    stop = MAX_CYCLES
    cycles = int(op_len / stop)
    some_owner_projects = []
    
    if cycles == 0:
        procs_dict = build_procs_dict(owner_projects=owner_projects, num_procs=NUM_PROCS)
        procs_dict = do_multiprocess(procs_dict=procs_dict)
        close_procs(procs_dict=procs_dict)
        dump_procs_dict(procs_dict=procs_dict)
        sys.exit(0)

    for x in range(cycles):
        if stop >= op_len:
            some_owner_projects = owner_projects[start:op_len].copy()
        if stop < op_len:
            some_owner_projects = owner_projects[start:stop].copy()
        stop += MAX_CYCLES
        start += MAX_CYCLES
        procs_dict = build_procs_dict(owner_projects=some_owner_projects, num_procs=NUM_PROCS)
        procs_dict = do_multiprocess(procs_dict=procs_dict)
        close_procs(procs_dict=procs_dict)
        dump_procs_dict(procs_dict=procs_dict)
        sleep(WAIT)
        some_owner_projects = []
    
    sys.exit(0)


    
    
if __name__ == '__main__':
  main()
