# README #

## What is this repository for? ##

**WARNING This is proof of concept code not intended for production due to inefficiencies.**

* A python wrapper to call github_extractor utilizing multiprocessing. In the near future github_extractor will be replaced by OSSBOM.
* Version 0.2.0
* Dependencies
    + [github-linguist](https://github.com/github/linguist/) 
    + [cyclonedx-cli](https://github.com/CycloneDX/cyclonedx-cli)
    + ./bin/github_extractor
    + ./bin/sbom_converter
    + ./bin/create_sbom.sh
    + zip
    + grep
    + git
    + python3
    + diff
    + jq
* Testing environment
    + XUbuntu 20.04
    + Python 3.8.10
    + cyclonedx-cli 0.21.0
    + Linguist v7.18.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##

* Pull this repository to a linux Ubuntu system.
* Install then add all Dependencies to environment path
    + [Ref. Docker Ubuntu Install](https://docs.docker.com/engine/install/ubuntu/)
    + [Ref. Github Linguist Install](https://github.com/github/linguist/)
    + [Ref. CycloneDX CLI binaries](https://github.com/CycloneDX/cyclonedx-cli/releases)
```
# -- The following command line steps will get you setup on Ubuntu 20.04 --

# copy binaries to /usr/bin/
sudo cp ./bin/github_extractor ./bin/sbom_converter ./bin/cyclonedx-cli /usr/bin/

# Make sure ./bin/create_sbom.sh has execute permissions
sudo chmod u+x ./bin/create_sbom.sh

# check dependencies
which cyclonedx-cli docker github-linguist github_extractor sbom_converter zip git diff python3 jq

# should get output similar to below
/usr/bin/cyclonedx-cli
/usr/bin/docker
/usr/local/bin/github-linguist
/usr/bin/github_extractor
/usr/bin/sbom_converter
/usr/bin/zip
/usr/bin/git
/usr/bin/diff
/usr/bin/python3

# -- Below are steps to install dependencies --

# update
sudo apt-get update && sudo apt-get upgrade -y 

# install dependencies
sudo apt-get install \
    python3 \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    git \
    zip \
    diff \
    cmake \
    pkg-config \
    libicu-dev \
    zlib1g-dev \
    libcurl4-openssl-dev \
    libssl-dev \
    ruby-dev \
    jq

# install github-linguist
gem install github-linguist

# add Docker gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# get Docker stable
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# install Docker
sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io

# Additional configurations steps are next.
```

* Update your username and/or email in your [Git configuration](https://git-scm.com/docs/git-commit#_commit_information)
```
git config --global user.name = "<your_username>"
git config --global user.email "<your_email>"
```

* Backup ./bin/config.json and create new with github.token GITHUB_ACCESS_TOKEN changed to your Github access token.
```
    },
  "github": {
	"token": "<GITHUB_ACCESS_TOKEN>",...
```

## Examples ##

The following are example run use cases with preceded by explanations of the command line flags.

* Get help
```
python3 ./invoke_github_extractor.py -h
```

* Get SBoMs on CSV file of sample owner projects, use 2 process, use config.json, set working directory to ./data, save SBoMs to ./sboms/ folder.
    * File ./data/invoke_github_extractor_results_<timestamp>.json will contain run time statistics
    * Two SBoMs are created for each project
        * One SBoM for the project in both XML and JSON named <owner>_<project>_sbom.<XML|JSON>
    * The working directory ./data/ will contain one or more folders with names as a bunch of numbers. Under those directories is where all the source code is downloaded and processed along with github_extractor output.
    * Command *python3 "./invoke_github_extractor.py"* means run the invoke_github_extractor.py script
        * Flag *-i "./sample_owner_project.csv"* means process the csv file of sample owner projects
        * Flag *-n 2* means use 2 system cores for multiprocessing
        * Flag *-c "./config/config.json"* means use file config.json for github_extractor
        * Flag *-w "./data/"* means use data directory for downloading source code and github_extractor output
        * Flag *-s "./sboms/"* means copy SBoMs to the sboms directory
```
python3 "./invoke_github_extractor.py" -i "./sample_owner_project.csv" -n 2 -c "./config/config.json" -w "./data/" -s "./sboms/"
```


* Using ssh and Linux Screen to run invoke_github_extractor on remote server
    * Connect over ssh
    * Start screen with name "ige"
    * Run invoke_github_extractor.py, for CSV repos_more, keep repository and output as zip files, use 6 cores, set working directory to runRM3,copy SBoMs to runRM3/sboms, save log output to runRM3_stdout_stderr.txt
    * Hit keyboard keys [crtl+a] [d] to detach the screen
    * Re-attach screen
```
ssh -v <username>@<host>
screen -S ige
python3 "./invoke_github_extractor.py" -i "./data/repos_more.csv" -k -n 6 -w "./data/repos_more/" -s "./data/sboms" &> ./data/runRM3_stdout_stderr.txt
[crtl+a] [d] #detach from screen
screen -r ige #re-attach screen
```


## What it does. ##

Invoke Github Extractor takes in a CSV of owner,projects and calls github_extractor utilizing multiprocessing. The github_extractor tool will pull the repository down from Github and attempt to generate SBoMs.

Since the github_extractor tool generates more than one SBoMs depending on the situation, Invoke Github Extractor generates several SBoMs at various stages of added data for a single repository.

* Execution stats
    + More processes for a single repository has no affect.
    + Running on 2 core XUbuntu 20.04 virtual machine, 81 repositories with 2 process = 33082 sec/81 repositories ~ 8 min/repository
    + Running on 8 core Ubuntu 20.04 server, 81 repositories, with 8 processes = 7864 sec/81 repositories ~ 2 min/repository
* Output Creates and validates 2 SBoMs.
    + SBoMs from source repository
        - \<owner\>\_\<project\>\_sbom.xml
        - \<owner\>\_\<project\>\_sbom.json

Out of 81 repositories tested 11 failed due to unsupported languages or errors processing project config files. 
The table below details the 11/81 owner projects that github_extractor failed to produce SBoMs.

| Owner Project | Reason |
| :---  | :---  |
| CocoaLumberjack CocoaLumberjack SBoMs fail | UNSUPPORTED languages |
| Meituan-Dianping Robust SBoMs fail | NOT ABLE TO generate CycloneDX BOM file from Gradle |
| dotnet core SBoMs fail | UNSUPPORTED languages |
| facebook fishhook SBoMs fail | UNSUPPORTED languages |
| gradle kotlin-dsl-samples SBoMs fail | UNSUPPORTED languages |
| igorwojda android-showcase SBoMs fail | failed to build Kotlin (UNSUPPORTED language) and Gradle |
| ja-netfilter ja-netfilter SBoMs fail | Could not generate CycloneDX BOM file from Maven POM file |
| laravel laravel SBoMs fail | NOT ABLE TO generate CycloneDX BOM file from PHP project |
| serhii-londar open-source-mac-os-apps SBoMs fail | UNSUPPORTED languages |
| twbs ratchet SBoMs fail | NOT ABLE TO generate CycloneDX BOM file from NodeJS projectRun |
| vsouza awesome-ios SBoMs fail | UNSUPPORTED languages |

### What the source code does. ###

* main invoke_github_extractor.py
    + multiprocess CreateGithubExtractorSBOM
    + some high level checks for SBoM generation failure
    + move SBoMs to sbom directory
* class CreateGithubExtractorSBOM
    + Setup and test the environment to run create_sbom.sh for single project
    + calls create_sbom.sh
* script create_sbom.sh
    + Clones repository
    + Tracks repository changes before and after github_extractor
        - \<owner\>\_\<project\>\_diff.txt
    + Calls github_extractor 
        - logs file \<owner\>\_\<project\>\_output/delacoll/github_extractor.log
            + same data in CreateGithubExtractorSBOM.run_completed_process.stdout
    + Reorganizes data and standardize file names
    + Creates and validates 2 SBoMs 
        + SBoMs from source repository
            - \<owner\>\_\<project\>\_sbom.xml
            - \<owner\>\_\<project\>\_sbom.json
    + Consolidated information saved to file and includes
        - File \<owner\>\_\<project\>\_output/\<owner\>_\<project\>_notes.txt
        - High level actions
        - Source code breakdown
        - SBoM validation results
        - File breakdown
        - ShiftLeft scan hits
        - Notable entries from github_extractor debug log
        - Various SBoM hashes
    + Zips output and deletes repository

## Who do I talk to? ##

* Repo owner [David Lambert](dlambert@fortressinfosec.com)
* Other community or team contact team VulnFort under [Tony Turner](tturner@fortressinfosec.com), or [Jace Powell](jpowell@fortressinfosec.com)

## Bugs ##

* FIXED - incorrect values for invoke_github_extractor_results_20220205T004138.json\['process'\] - now shows return code of process
* FIXED - missing files \*final_sboms.* results
* FIXED - create_sboms.sh notes.txt fail - bad mv command
* FIXED - not copy to sboms dir
* FIXED - output files *_notes.txt moved to wrong location, should be in process sub directory
* FIXED - many process working directories
    + should be 8 directories for 8 core run.
* FIXED - some no sbom results
    + due to unsupported languages or file processing error.
* FIXED - some missing repository file inventory SBoMs
    + new observed github_extractor write folder "node_modules" to repository causing permission failures


