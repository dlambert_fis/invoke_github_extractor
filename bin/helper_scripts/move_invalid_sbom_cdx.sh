#!/bin/bash

# add some metadata to sbom.json
dest="./invalid_FOSS_SBoMs"
while getopts f:d: flag
do
    case "${flag}" in
        f) file=${OPTARG};;
        d) dest=${OPTARG};;
    esac
done


file=$(realpath ${file})
json=$(cat ${file})
dest=$(realpath ${dest})
mkdir -p "${dest}"

check_valid=$(echo "$json" | cyclonedx-cli validate --input-format json | grep -o 'BOM validated successfully.')

if [[ "${check_valid}" == 'BOM validated successfully.' ]];then
    echo "Valid: ${file}"
    exit 0
fi

echo "Move: ${file} --> ${dest}"
mv "${file}" "${dest}"
exit 0
