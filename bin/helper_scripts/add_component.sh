#!/bin/bash

# expecting file name format <owner>_<project>_<OPTIONAL_version>_sbom.json

# find . -iname "*_sbom.json" -exec ./add_component.sh -f {} \;

tag='_sbom.json'
bad="./bad"
while getopts f:b: flag
do
    case "${flag}" in
        f) file=${OPTARG};;
        b) bad=${OPTARG};;
    esac
done


file=$(realpath ${file})
file_name=$(basename ${file})
bad=$(realpath ${bad})
has_component=$(cat "${file}" | jq '.metadata? | has("component")' | grep true)

if [[ ${has_component} == true ]];then 
    echo "Has component: ${file}"
    exit 0
fi

us="${file_name//[^_]}"
num_underscores=$(grep -o '_' <<< "$us" | grep -c .)

if [[ $num_underscores -lt 2 ]];then
    if [[ ! -d "${bad}" ]];then
        mkdir -p "${bad}"
    fi
    echo "Move bad: ${file}"
    mv "${file}" "${bad}"
    exit 0 
fi
if [[ $num_underscores -ge 4 ]];then
    if [[ ! -d "${bad}" ]];then
        mkdir -p "${bad}"
    fi
    echo "Move bad: ${file}"
    mv "${file}" "${bad}"
    exit 0
fi

IFS='_'
read -ra array_file_name <<< "${file_name}"
owner="${array_file_name[0]}"
project="${array_file_name[1]}"
version="${array_file_name[2]}"

if [[ ${version} == 'sbom.json' ]];then
    version=''
fi

component="{\"component\":{\"type\":\"application\",\"name\":\"${project}\",\"version\":\"${version}\",\"group\":\"${owner}\"}}"

echo "Add component: ${file}"
cat "${file}" | jq ".metadata += ${component}" > edit.json
mv "edit.json" "${file}"
sleep 1
exit 0

