#!/bin/bash

# example fail
# ./bin/create_sbom.sh -c ./config/config.json -d ./data/ -o "ja-netfilter" -p "ja-netfilter"

# example 
# ./bin/create_sbom.sh -c ./config/config.json -d ./data/ -o "dropwizard" -p "dropwizard"

vendor_tag='{"vendor":"Fortress Information Security","name":"GitHub Extractor","version":"1.15.1"}'
tools_tag="{\"tools\": [${vendor_tag}]}"

name_tag='{"name": "Fortress Information Security", "email": "sales@fortressinfosec.com", "phone": "1-407-573-6800"}'
authors_tag="{\"authors\": [${name_tag}]}"
metadata_tag_author="{\"metadata\": ${authors_tag} }"

now="$(date -I'seconds' -u | sed 's/\+00\:00/Z/g')"
timestamp="{\"timestamp\":\"${now}\"}"

while getopts c:d:o:p: flag
do
    case "${flag}" in
        c) config=${OPTARG};;
        d) dir=${OPTARG};;
        o) owner=${OPTARG};;
        p) project=${OPTARG};;
       
    esac
done

config=$(realpath ${config})
dir=$(realpath ${dir})

op="${owner}_${project}"
opoutput="${op}_output"
opnotes="${op}_notes.txt"
opsrcbd="${op}_src_breakdown.json"
opflbefore="${op}_fl.txt"
opflafter="${op}_fl_after.txt"
opfldiff="${op}_diff.txt"
opaddfltxt="${op}_add_files.txt"
opxml="${op}_sbom.xml"
opjson="${op}_sbom.json"
# opwflxml="${op}_sbom_with_files.xml"
# opwfljson="${op}_sbom_with_files.json"
opreportwflxml="${op}_report_sbom_with_files.xml"
opreportwfljson="${op}_report_sbom_with_files.json"
opreportxml="${op}_report_sbom.xml"
opreportjson="${op}_report_sbom.json"
opfilelistxml="${op}_files.xml"
opfilelistjson="${op}_files.json"
opmergedxml="${op}_merged_sbom.xml"
opmergedjson="${op}_merged_sbom.json"

ghurl="https://github.com/${owner}/${project}"
giturl="${ghurl}.git"
exter_ref="{\"url\":\"${giturl}\",\"type\":\"distribution\"}"
exter_ref_array="{\"externalReferences\":[${exter_ref}]}"
# check for both project-sbom.xml and bom-report.xml
havesbom='false'
havereportsbom='false'

here=$(pwd)

cd "${dir}"
echo "$(date --iso-8601=seconds)|Starting in dir ${dir}" >> "./${opnotes}"

echo "$(date --iso-8601=seconds)|Getting ${owner} ${project}" >> "./${opnotes}"
echo "$(date --iso-8601=seconds)|Getting Repository ${ghurl}.git" >> "./${opnotes}"
git clone -q "${ghurl}.git"
sleep 0.1

echo "$(date --iso-8601=seconds)|Record repository files" >> "./${opnotes}"
ls -hal ./${project}/ > ${opflbefore}

echo "$(date --iso-8601=seconds)|Run github_extractor" >> "./${opnotes}"
# github_extractor -v --contributors  --commits --input-folder "./${project}/" -c ${config} -o "./${opoutput}" #contributer not processed
github_extractor -v --contributors --commits --input-folder "./${project}/" -c ${config} -o "./${opoutput}"
sleep 0.1

### comment unnessary
# echo "$(date --iso-8601=seconds)|Getting source code breakdown" >> "./${opnotes}"
# github-linguist "./${project}/" >> "./${opnotes}"
# github-linguist -b -j "./${project}/" > "./${opoutput}/${opsrcbd}"

# echo "$(date --iso-8601=seconds)|Record repository files and diff" >> "./${opnotes}"
# ls -hal "./${project}/" > "./${opflafter}"

# diff -y --suppress-common-lines "./${opflbefore}" "./${opflafter}" > "./${opfldiff}"


# echo "$(date --iso-8601=seconds)|Clean up github_extractor data folder named ? " >> "./${opnotes}"
# [ -d "./${project}/?" ] && rm -r "./${project}/?"
### comment unnessary end
echo "$(date --iso-8601=seconds)|Moving github_extractor data out of repository and organize other output" >> "./${opnotes}"
[ -d "./${project}/scan-report" ] && mv "./${project}/scan-report" "./${opoutput}/delacoll/github_extractor/"
[ -d "./${project}/target" ] && mv "./${project}/target" "./${opoutput}/delacoll/github_extractor/"
[ -d "./${project}/node_modules" ] && mv "./${project}/node_modules" "./${opoutput}/delacoll/github_extractor/"
[ -f "./${project}/bom.xml" ] && mv "./${project}/bom.xml" "./${opoutput}/delacoll/github_extractor/"
[ -f "./${project}/package_lock.json" ] && mv "./${project}/package_lock.json" "./${opoutput}/delacoll/github_extractor/"
[ -f "./${opflbefore}" ] && mv "./${opflbefore}" "./${opoutput}/delacoll/github_extractor/"
[ -f "./${opflafter}" ] && mv "./${opflafter}" "./${opoutput}/delacoll/github_extractor/"
[ -f "./${opfldiff}" ] && mv "./${opfldiff}" "./${opoutput}/delacoll/github_extractor/"
sleep 0.1
### comment unnessary
# # create file list, cyclonedx-cli follows symbolic links, remove symbolic links before adding files
# echo "$(date --iso-8601=seconds)|Remove symbolic links from repository ./${project}/" >> "./${opnotes}"
# find "./${project}/" -type l >> "./${opnotes}"
# find "./${project}/" -type l -exec rm -f {} \;


# cyclonedx-cli add files --no-input --output-format xml --base-path "./transformers/" --exclude /.git/** --output-file "./huggingface_transformers_filelist.xml"
# echo "$(date --iso-8601=seconds)|Create XML file list of repository ./${opoutput}/${opfilelistxml}" | tee -a "./${opoutput}/delacoll/github_extractor/${opaddfltxt}" >> "./${opnotes}"
# cyclonedx-cli add files --no-input --output-format xml --base-path "./${project}/" --exclude /.git/** --output-file "./${opoutput}/${opfilelistxml}" >> "./${opoutput}/delacoll/github_extractor/${opaddfltxt}"
# sleep 0.1

# echo "$(date --iso-8601=seconds)|Create JSON file list of repository ./${opoutput}/${opfilelistjson}" | tee -a "./${opoutput}/delacoll/github_extractor/${opaddfltxt}" >> "./${opnotes}"
# cyclonedx-cli add files --no-input --output-format json --base-path "./${project}/" --exclude /.git/** --output-file "./${opoutput}/${opfilelistjson}" >> "./${opoutput}/delacoll/github_extractor/${opaddfltxt}"
# sleep 0.1
### comment unnessary end

if [ -f "./${opoutput}/delacoll/github_extractor/${project}-sbom.xml" ];then
    havesbom='true'
    echo "$(date --iso-8601=seconds)|Found ${project}-sbom.xml" >> "./${opnotes}"
    
    echo "$(date --iso-8601=seconds)|Validate ${project}-sbom.xml using CycloneDX-cli" >> "./${opnotes}"
    cyclonedx-cli validate --input-format xml --input-file "./${opoutput}/delacoll/github_extractor/${project}-sbom.xml" >> "./${opnotes}"


    echo "$(date --iso-8601=seconds)|Convert ./${opoutput}/delacoll/github_extractor/${project}-sbom.xml to CycloneDX v1.3 JSON./${opoutput}/${opjson}" >> "./${opnotes}"
    cyclonedx-cli convert --input-format xml --input-file "./${opoutput}/delacoll/github_extractor/${project}-sbom.xml" --output-format json --output-version v1_3 --output-file "./${opoutput}/${opjson}"
    sleep 0.1

    # company tags, do json in memory
    echo "$(date --iso-8601=seconds)|Add metadata to ./${opoutput}/${opjson}" >> "./${opnotes}"
    json=$(cat "./${opoutput}/${opjson}" | jq '.')
    
    has_metadata=$(echo "$json" | jq '. | has("metadata")'| grep true | head -1)
    if [[ $has_metadata != true ]];then
        json=$(echo "$json" | jq ". += ${metadata_tag_author}")
        has_metadata=$(echo "$json" | jq '. | has("metadata")'| grep true | head -1)
    fi

    if [[ $has_metadata == true ]];then
        # have metadata, check metadata.tools,
        has_tools=$(echo "$json" | jq '.metadata | has("tools")' | grep true | head -1)
        if [[ $has_tools == true ]];then

            contains_fortress_vendor=$(echo $json | jq '.metadata.tools[].vendor | contains("Fortress Information Security")' | grep true | head -1)
            if [[ $contains_fortress_vendor != true ]];then
                json=$(echo "$json" | jq ".metadata.tools[.metadata.tools|length] += ${vendor_tag}") 
            fi
        fi
        if [[ $has_tools != true ]];then
            # add tools[] and $tag
            json=$(echo "$json" | jq ".metadata += ${tools_tag}") 
        fi

        # check metadata.authors
        has_authors=$(echo $json | jq '.metadata | has("authors")'| grep true | head -1)
        if [[ $has_authors != true ]];then
            json=$(echo "$json" | jq ".metadata += ${authors_tag}")
        fi

        if [[ $has_authors == true ]];then
            contains_fortress_author=$(echo $json | jq '.metadata.authors[].name | contains("Fortress Information Security")' | grep true | head -1)
            if [[ $contains_fortress_author != true ]];then
                json=$(echo "$json" | jq ".metadata.authors[.metadata.authors|length] += ${name_tag}")
            fi
        fi

        has_meta_ts=$(echo "$json" | jq '.metadata | has("timestamp")' | grep true | head -1)
        if [[ ${has_meta_ts} != true ]];then
            json=$(echo "$json" | jq ".metadata += ${timestamp}")
        fi
    fi # end if [[ $has_metadata == true ]]

    # metadata should exist
    # add component if not included
    page=$(curl -sSfJL "${ghurl}")
    cexit=$?
    has_component=$(echo "${json}" | jq '.metadata? | has("component")' | grep true)
    
    if [[ ${has_component} != true ]];then
        ghrpc=$(echo "${page}" | grep 'repo-content-pjax-container')
        ghrpcexit=$?
        if [[ ${ghrpcexit} -eq 0 ]];then
            ghversion=$(echo "${ghrpc}" | grep -oP '(?<=releases\/tag\/).+(?=\"\>)')
            ghversionexit=$?
            if [[ ${ghversionexit} -eq 0 ]];then
                version="${ghversion}"
            fi
            if [[ ${ghversionexit} != 0 ]];then
                version=''
            fi
        fi
        if [[ ${ghrpcexit} != 0 ]];then
            version=''
        fi        
        component="{\"component\":{\"type\":\"application\",\"name\":\"${project}\",\"version\":\"${version}\",\"group\":\"${owner}\"}}"
        json=$(echo "${json}" | jq ".metadata += ${component}")
        
    fi
    # todo when has_component == true add check for metadata.component.group exists if not add group, check name, if not add


    # add external references
    has_exter_ref_array=$(echo ${json} | jq '.metadata.component | has("externalReferences")' | grep "true" | head -1)
    if [[ ${has_exter_ref_array} != true ]];then
        json=$(echo "${json}" | jq ".metadata.component += ${exter_ref_array}")
    fi

    if [[ ${has_exter_ref_array} == true ]];then
        has_exter_ref=$(echo ${json} | jq ".metadata.component.externalReferences[]?.url | contains(\"${giturl}\")" | grep "true" | head -1)
        if [[ ${has_exter_ref} != true ]];then
            json=$(echo "${json}" | jq ".metadata.component.externalReferences[.metadata.component.externalReferences|length] += ${exter_ref}")
        fi
        
    fi

    echo "${json}" > "./${opoutput}/${opjson}"
    # end company tags
    
    echo "$(date --iso-8601=seconds)|Convert ./${opoutput}/${opjson} to XML " >> "./${opnotes}"
    cyclonedx-cli convert --input-format json --input-file "./${opoutput}/${opjson}" --output-format xml --output-version v1_3 --output-file "./${opoutput}/${opxml}"
    sleep 0.1

    echo "$(date --iso-8601=seconds)|Check if empty ./${opoutput}/${opjson}" >> "./${opnotes}"
    numComponents=$(jq '.components[]?.purl?' "./${opoutput}/${opjson}" | wc -l)
    if [[ numComponents -eq 0 ]];then
        echo "$(date --iso-8601=seconds)|Is empty ./${opoutput}/${opjson} move SBoMs to ./${opoutput}/fail/" >> "./${opnotes}"
        mkdir -p "./${opoutput}/fail/"
        mv "./${opoutput}/${opjson}" "./${opoutput}/${opxml}" "./${opoutput}/fail/"
    fi
fi

if [ -f "./${opoutput}/delacoll/github_extractor/bom-report.xml" ];then
    havereportsbom='true'
    echo "$(date --iso-8601=seconds)|Found bom-report.xml" >> "./${opnotes}"
    
    echo "$(date --iso-8601=seconds)|Validate bom-report.xml using CycloneDX-cli" >> "./${opnotes}"
    cyclonedx-cli validate --input-format xml --input-file "./${opoutput}/delacoll/github_extractor/bom-report.xml" >> "./${opnotes}"
    
    echo "$(date --iso-8601=seconds)|Convert ./${opoutput}/delacoll/github_extractor/bom-report.xml to CycloneDX v1.3 JSON ./${opoutput}/${opreportjson}" >> "./${opnotes}"
    cyclonedx-cli convert --input-format xml --input-file "./${opoutput}/delacoll/github_extractor/bom-report.xml" --output-format json --output-version v1_3 --output-file "./${opoutput}/${opreportjson}"
    sleep 0.1

    # company tags
    echo "$(date --iso-8601=seconds)|Add metadata to ./${opoutput}/${opreportjson}" >> "./${opnotes}"
    json=$(cat "./${opoutput}/${opreportjson}" | jq '.')
    
    has_metadata=$(echo "$json" | jq '. | has("metadata")'| grep true | head -1)
    if [[ $has_metadata != true ]];then
        json=$(echo "$json" | jq ". += ${metadata_tag_author}")
        has_metadata=$(echo "$json" | jq '. | has("metadata")'| grep true | head -1)
    fi

    if [[ $has_metadata == true ]];then
        # have metadata, check metadata.tools,
        has_tools=$(echo "$json" | jq '.metadata | has("tools")' | grep true | head -1)
        if [[ $has_tools == true ]];then

            contains_fortress_vendor=$(echo $json | jq '.metadata.tools[].vendor | contains("Fortress Information Security")' | grep true | head -1)
            if [[ $contains_fortress_vendor != true ]];then
                json=$(echo "$json" | jq ".metadata.tools[.metadata.tools|length] += ${vendor_tag}") 
            fi
        fi
        if [[ $has_tools != true ]];then
            # add tools[] and $tag
            json=$(echo "$json" | jq ".metadata += ${tools_tag}") 
        fi

        # check metadata.authors
        has_authors=$(echo $json | jq '.metadata | has("authors")'| grep true | head -1)
        if [[ $has_authors != true ]];then
            json=$(echo "$json" | jq ".metadata += ${authors_tag}")
        fi

        if [[ $has_authors == true ]];then
            contains_fortress_author=$(echo $json | jq '.metadata.authors[].name | contains("Fortress Information Security")' | grep true | head -1)
            if [[ $contains_fortress_author != true ]];then
                json=$(echo "$json" | jq ".metadata.authors[.metadata.authors|length] += ${name_tag}")
            fi
        fi

        has_meta_ts=$(echo "$json" | jq '.metadata | has("timestamp")' | grep true | head -1)
        if [[ ${has_meta_ts} != true ]];then
            json=$(echo "$json" | jq ".metadata += ${timestamp}")
        fi
    fi # end if [[ $has_metadata == true ]]

    # metadata should exist
    # add component if not included
    page=$(curl -sSfJL "${ghurl}")
    cexit=$?
    has_component=$(echo "${json}" | jq '.metadata? | has("component")' | grep true)
    
    if [[ ${has_component} != true ]];then
        ghrpc=$(echo "${page}" | grep 'repo-content-pjax-container')
        ghrpcexit=$?
        if [[ ${ghrpcexit} -eq 0 ]];then
            ghversion=$(echo "${ghrpc}" | grep -oP '(?<=releases\/tag\/).+(?=\"\>)')
            ghversionexit=$?
            if [[ ${ghversionexit} -eq 0 ]];then
                version="${ghversion}"
            fi
            if [[ ${ghversionexit} != 0 ]];then
                version=''
            fi
        fi
        if [[ ${ghrpcexit} != 0 ]];then
            version=''
        fi        
        component="{\"component\":{\"type\":\"application\",\"name\":\"${project}\",\"version\":\"${version}\",\"group\":\"${owner}\"}}"
        json=$(echo "${json}" | jq ".metadata += ${component}")
        
    fi
    
    # todo when has_component == true add check for metadata.component.group exists if not add group, check name, if not add

    # add external references
    has_exter_ref_array=$(echo ${json} | jq '.metadata.component | has("externalReferences")' | grep "true" | head -1)
    if [[ ${has_exter_ref_array} != true ]];then
        json=$(echo "${json}" | jq ".metadata.component += ${exter_ref_array}")
    fi

    if [[ ${has_exter_ref_array} == true ]];then
        has_exter_ref=$(echo ${json} | jq ".metadata.component.externalReferences[]?.url | contains(\"${giturl}\")" | grep "true" | head -1)
        if [[ ${has_exter_ref} != true ]];then
            json=$(echo "${json}" | jq ".metadata.component.externalReferences[.metadata.component.externalReferences|length] += ${exter_ref}")
        fi
        
    fi

    echo "${json}" > "./${opoutput}/${opreportjson}"
    # end company tags
    
    echo "$(date --iso-8601=seconds)|Convert ./${opoutput}/${opreportjson} to XML " >> "./${opnotes}"
    cyclonedx-cli convert --input-format json --input-file "./${opoutput}/${opreportjson}" --output-format xml --output-version v1_3 --output-file "./${opoutput}/${opreportxml}"
    sleep 0.1

    echo "$(date --iso-8601=seconds)|Check if empty ./${opoutput}/${opreportjson}" >> "./${opnotes}"
    numComponents=$(jq '.components[]?.purl?' "./${opoutput}/${opreportjson}" | wc -l)
    if [[ numComponents -eq 0 ]];then
        echo "$(date --iso-8601=seconds)|Is empty ./${opoutput}/${opreportjson} move SBoMs to ./${opoutput}/fail/" >> "./${opnotes}"
        mkdir -p "./${opoutput}/fail/"
        mv "./${opoutput}/${opreportjson}" "./${opoutput}/${opreportxml}" "./${opoutput}/fail/"
    fi
fi    


# # have sbom add files
# if [ -f "./${opoutput}/${opxml}" ];then
    
#     # add files
#     echo "$(date --iso-8601=seconds)|Using cyclonedx add files to ./${opoutput}/${opxml} as ./${opoutput}/${opwflxml}" | tee -a "./${opoutput}/delacoll/github_extractor/${opaddfltxt}" >> "./${opnotes}"
#     cyclonedx-cli add files --input-file "./${opoutput}/${opxml}" --input-format xml --output-format xml --base-path "./${project}/" --exclude /.git/** --output-file "./${opoutput}/${opwflxml}" >> "./${opoutput}/delacoll/github_extractor/${opaddfltxt}"
#     sleep 0.1
    
#     echo "$(date --iso-8601=seconds)|Validate ./${opoutput}/${opwflxml}" >> "./${opnotes}"
#     cyclonedx-cli validate --input-format xml --input-file "./${opoutput}/${opwflxml}" >> "./${opnotes}"
    
#     echo "$(date --iso-8601=seconds)|Convert ./${opoutput}/${opwflxml} to CycloneDX v1.3 to JSON ./${opoutput}/${opwfljson}" >> "./${opnotes}"
#     cyclonedx-cli convert --input-format xml --input-file "./${opoutput}/${opwflxml}" --output-format json --output-version v1_3 --output-file "./${opoutput}/${opwfljson}"
#     sleep 0.1

# fi
# # have sbom report add files
# if [ -f "./${opoutput}/${opreportxml}" ];then
    
#     # add files way is _final_sbom.xml
#     echo "$(date --iso-8601=seconds)|Using cyclonedx add files to ./${opoutput}/${opreportxml} as ./${opoutput}/${opreportwflxml}" | tee -a "./${opoutput}/delacoll/github_extractor/${opaddfltxt}" >> "./${opnotes}"
#     cyclonedx-cli add files --input-file "./${opoutput}/${opreportxml}" --input-format xml --output-format xml --base-path "./${project}/" --exclude /.git/** --output-file "./${opoutput}/${opreportwflxml}" >> "./${opoutput}/delacoll/github_extractor/${opaddfltxt}"
#     sleep 0.1

#     echo "$(date --iso-8601=seconds)|Validate ./${opoutput}/${opreportxml}" >> "./${opnotes}"
#     cyclonedx-cli validate --input-format xml --input-file "./${opoutput}/${opreportxml}" >> "./${opnotes}"
    
#     echo "$(date --iso-8601=seconds)|Convert ./${opoutput}/${opreportxml} to CycloneDX v1.3 to JSON ./${opoutput}/${opreportwfljson}" >> "./${opnotes}"
#     cyclonedx-cli convert --input-format xml --input-file "./${opoutput}/${opreportxml}" --output-format json --output-version v1_3 --output-file "./${opoutput}/${opreportwfljson}"
#     sleep 0.1
# fi


# organize data
echo "$(date --iso-8601=seconds)|Move dependencies, shiftleft, logs, and sboms .json and .xml up" >> "./${opnotes}"
[ -f "./${opoutput}/delacoll/github_extractor/${project}-report/scan-full-report.json" ] && mv "./${opoutput}/delacoll/github_extractor/${project}-report/scan-full-report.json" "./${opoutput}/"
[ -f "./${opoutput}/delacoll/github_extractor/${project}-dependencies.csv" ] && mv "./${opoutput}/delacoll/github_extractor/${project}-dependencies.csv" "./${opoutput}/"
[ -f "./${opoutput}/delacoll/github_extractor.log" ] && mv "./${opoutput}/delacoll/github_extractor.log" "./${opoutput}/${op}_github_extractor.log"
#[ -f "./${opoutput}/${op}_github_extractor.log" ] && mv "./${opoutput}/${op}_github_extractor.log" "./${opoutput}/"
     
### comment unnessary
# echo "$(date --iso-8601=seconds)|Fin" >> "./${opnotes}"
# echo ""
# echo  '''==== file breakdown ====================
# <owner>_<project>_<version>_sbom.*          = SBoM generated
# <owner>_<project>_src_breakdown.json        = List of repository files grouped by language
# <owner>_<project>-dependencies.csv          = List of dependencies and tool that generated them
# scan-full-report.json                       = ShiftLeft full report in JSON format
#     (html versions in directory /delacoll/github_extractor/<project>-report/)
# ''' >> "./${opnotes}"

# echo "\n==== ShiftLeft Scan report hits ====================" >> "./${opnotes}"
# find ./${opoutput}/delacoll/github_extractor/${project}-report/ -type f -iname *.html -size +17k >> "./${opnotes}"


# ghl=$(find . -type f -iname "*github_extractor.log")
# echo "\n==== noteable from $ghl ====" >> "./${opnotes}"
# grep -iE "(Executing|\"Warning\"|\"Error\")" $ghl | grep -v "\u001b" >> "./${opnotes}"

# echo "\n==== XML file hashes ====" >> "./${opnotes}"
# find ./${opoutput}/delacoll/ -type f -iname "*.xml" -exec openssl md5 {} \; >> "./${opnotes}"


# echo "" >> "./${opnotes}"
### comment unnessary end

# zip output 
echo "$(date --iso-8601=seconds)|Zip output" >> "./${opnotes}"
zip -q -r "./${opoutput}.zip" "./${opoutput}/"
sleep 0.1

echo "$(date --iso-8601=seconds)|Zip repository" >> "./${opnotes}"
zip -q -r "./${owner}_${project}_repository.zip" "./${project}/"
sleep 0.1

if [[ ${havesbom} == 'false' && ${havereportsbom} == 'false' ]];then
    echo "$(date --iso-8601=seconds)|Fail SBoMs ${owner}/${project}" >> "./${opnotes}"
    echo "Fail SBoMs ${owner}/${project}"
fi

echo "$(date --iso-8601=seconds)|Fin" >> "./${opnotes}"
cd "${here}"
exit 0
