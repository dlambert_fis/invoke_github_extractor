/*

 "bomFormat": "CycloneDX",
  "specVersion": "1.2"

Get number missing fields and if it is valid or not valid
Document baseline information about each component that should
be tracked: Supplier , Component Name, Version of the Component,
Other Unique Identifiers, Dependency Relationship, Author of
SBOM Data, and Timestamp

supplier -   "supplier": {
        "Name": "Igor Pavlov"
      },
      
*/


const yargs = require("yargs");
var fs = require("fs");
var jsonvalidate = require("jsonschema").validate;
var xmlValidate = require("xsd-schema-validator");
// var x = require("libxmljs");
//const { DOMParser } = require('xmldom')
var glob = require("glob");
const { help } = require("yargs");

let validated = false

const xmlValidator = (file, schemafile) => {
  console.log(file + " " + schemafile);
  
  xmlValidate.validateXML(file, schemafile, function (err, result) {
    if (err) {
      console.log(`Validation errors for ${file} ${schemafile}:\n\t` + err);
      return err.length;
    } else {
      console.log(result);
      console.log(
        `Input file ${file} successfully validated against schema file ${schemafile}`);
    }
  });
};


const jsonvalidator = (file, schemafile) => {
  console.log(file + " " + schemafile);
  let schemaStr = fs.readFileSync(schemafile, "utf8");
  let schema = JSON.parse(schemaStr);
  fs.readFile(file, function (err, fileContents) {
    // console.log(file);
    let data = JSON.parse(fileContents);
    //console.log(`This is ${data.bomFormat} ${data.specVersion}`);
    // const validator = ajv.compile(schema);

    let validatorResult = jsonvalidate(data, schema);

    // console.log(Object.keys(validatorResult.instance));
    // validatorResult.errors
    let arguments = []
    validatorResult.errors.forEach(error => arguments.push(error.argument.toLowerCase()));
    //check if CycloneDX vs SPDX
    if (arguments.length > 0) {
      if ((data.hasOwnProperty('bomFormat') && !arguments.includes('name', 'version', 'purl', 'ref', 'author', 'timestamp', 'dependsOn')) || (data.hasOwnProperty('spdxVersion') && !arguments.includes('supplier', 'name', 'versionInfo', 'externalDocumentId', 'spdxElementId', 'relationshipType', 'originator', 'created')))
      {
        console.log(
        //results are here
        `Validation errors for ${file} and ${schemafile}:\n\t` + validatorResult.errors.join("\n\t")
      );
      return validatorResult.errors.length
        } else {
      console.log(
        `Input file '${file}' successfully validated against schema file '${schemafile}'`
      );
    }
  }else{
    console.log(
      `There are no errors: Input file '${file}' successfully validated against schema file '${schemafile}'`
    );
  }
  });
};

yargs
  .command({
    command: "all [dirPath] [schemaFile]",
    description: "Validate all file for schema",
    builder: (yargs) => {
      return yargs
        .positional("dirPath", { describe: "directory to validate" })
        .positional("schemaFile", { describe: "Schemfile to match" });
    },
    handler: function (argv) {
      let fileType = "";
      if (argv.schemaFile.endsWith(".json")) fileType = "json";
      else if (argv.schemaFile.endsWith(".xsd")) fileType = ".xml";
      console.log(argv.dirPath + " " + argv.schemaFile + " " + fileType);
      glob(argv.dirPath + "/*" + fileType, function (err, files) {
        console.log(files);
        // read the folder or folders if you want: example json/**/*.json
        if (err) {
          console.log(
            "cannot read the folder, something goes wrong with glob",
            err
          );
        }
        files.forEach(function (file) {
          console.log(
            file + " " + file.endsWith(".json") + " " + argv.schemaFile
          );
          if (file.endsWith(".json")) jsonvalidator(file, argv.schemaFile);
          else if (file.endsWith(".xml")) xmlValidator(file, argv.schemaFile);
        });
      });
    },
  })
  .command({
    command: "file [dataFile] [schemaFile]",
    description: "Validate single file with schema",
    builder: (yargs) => {
      return yargs
        .positional("dataFile", { describe: "dataFile to validate" })
        .positional("schemaFile", { describe: "Schemfile to match" });
    },
    handler: function (argv) {
      try {
      validated = true
      console.log(argv.dataFile + " " + argv.schemaFile);
      if (argv.dataFile.endsWith(".json"))
        jsonvalidator(argv.dataFile, argv.schemaFile);
      else if (argv.dataFile.endsWith(".xml"))
        xmlValidator(argv.dataFile, argv.schemaFile);
      }
      catch(e){
        console.log(`Error with validation: ${e}`)
      }
      return validated
    },
  })
  .help()
  .alias("h", "help").argv;