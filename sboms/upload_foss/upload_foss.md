# upload foss #

## Summary ##
Processed SBoMs count: 6494
Success: 4353
Upload Success rate: 66%
Missing or Invalid root component version: 2141


File: /home/fis/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss/data/out_bulk_redo_all.psv
Processed SBoMs count: 6513
Success: 4353
Upload Success rate: 66%
Missing or Invalid root component version: 2141
No API response: 19

=============== No API response SBoMs ====================================
Shougo_deoplete.nvim_5.2_sbom.json

=============== Error (Must specify path or product) SBoMs =================
neon-bindings_neon_sbom.json


## prep ##

* folders include sboms from upload_foss_20220804
    * redo_empty_version = fail/no_root_version
    * redo_no_isSuccess
    * redo_product = includes fail/invalid_login, fail_dupe
    * redo_bad_group = edit to include component group and fix bom ref - done
* stats from upload_foss_20220804
    * done = 4361
    * fail = 2126
    * total = 6487 - 1 = 6486
        * fia_res_log.json = removed
* Getting 413 payload too big error for bulk vendor create - FIXED server side memory tuning
* ready to bulk upload to production
```
# split up file - done
find ./data/redo/ -iname "*.json" | head -n 500 | xargs -I '{}' mv '{}' ./data/redo_500/
find ./data/redo/ -iname "*.json" | head -n 1000 | xargs -I '{}' mv '{}' ./data/redo_1000/

```

## run ##

* start
```
cd ~/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss/

# login - 724c04ce1aaf3a0f7ca1cf4eb3222ef1ea12f31329af6714afce76d6a437e2f26a4cedfcc1d81845b29a852f1a4adce5
# 583bb0ece87e4b2d3d89ece18f20bd261afe65be62121dee460f8fe56f820b5dd1d67f29b8c74629eccfa22cf1055772
# 76d1b9167b38e26d8ec0e03d8212890c11b8fdf7b9595e7e4bba9333129d0a105570cc2a3a83255af289c95fb4396b4d
# 70de5676033009e48de22a402fd8413921c22d17008b6af6d823102d63099580e23f69c934ed8da2fcbe226a397e80b6
# 20220926 09959d9dc7f93ccc7f955dd68e6a7524a5d143fcc781cb37f49c00116c16d484ef10005fde6ddcae750ecc02e0e25700, 4c0c95dbcb3cb9ee57e1c4f7942884bc0044bd7f43b7a34226a5f96bd4e3d0ad63b61c5a588c2db364dde76d4b54e4c2,

cat ~/ws/OSSBOM/create_sboms/bin/.fia-session && echo '' && run_fia_cli.sh -l -e 'dlambert@fortressinfosec.com'
cat ~/ws/OSSBOM/create_sboms/bin/.fia-session && echo ''
```


* Some fails worth retry
```
find ./data/redo_1000_2/ -iname "*.json" -type f -exec run_fia_cli.sh -u -d -f {} \; &>> out_bulk_redo_noapires.psv
fruitcake_laravel-cors_v3.0.0_sbom.json
alecthomas_kingpin_v1.3.4_sbom.json
stedolan_jq_jq-1.6_sbom.json
fyne-io_fyne_v2.1.4_sbom.json
kizniche_Mycodo_v8.13.9_sbom.json
barryvdh_laravel-ide-helper_v2.12.3_sbom.json
jdkato_prose_v1.2.1_sbom.json
F5OEO_rpitx_v1_sbom.json
Shougo_deoplete.nvim_5.2_sbom.json
```


* Sboms that failed, most not have version string so not worth
```
find ./data/ -type f -name "Shougo_deoplete.nvim_5.2_sbom.json" -exec cp {} ./bulk_redo_noapires/ \;

cat ./data/out_bulk_redo_all.psv | grep 'No_isSuccess' | cut -d '|' -f 3 | xargs -I '{}' basename '{}'
fruitcake_laravel-cors_v3.0.0_sbom.json
alecthomas_kingpin_v1.3.4_sbom.json
bottlepy_bottle_sbom.json
nunomaduro_collision_sbom.json
cmusphinx_pocketsphinx_sbom.json
xbgmsharp_ipxe-buildweb_sbom.json
LionSec_xerosploit_sbom.json
stedolan_jq_jq-1.6_sbom.json
aimeos_aimeos-core_sbom.json
apache_incubator-weex_0.28.0_sbom.json
cloudera_livy_sbom.json
fyne-io_fyne_v2.1.4_sbom.json
kizniche_Mycodo_v8.13.9_sbom.json
barryvdh_laravel-ide-helper_v2.12.3_sbom.json
naiba_nezha_sbom.json
jdkato_prose_v1.2.1_sbom.json
pcwalton_sprocketnes_sbom.json
F5OEO_rpitx_v1_sbom.json
Shougo_deoplete.nvim_5.2_sbom.json
```


* upload all remaining
```
find ./data/redo_1000_2/ -iname "*.json" -type f -exec run_fia_cli.sh -u -d -f {} \; &>> out_bulk_upload_redo_1000_2.psv
find ./data/redo_1000_3/ -iname "*.json" -type f -exec run_fia_cli.sh -u -d -f {} \; &>> out_bulk_upload_redo_1000_3.psv
find ./data/redo_1000_4/ -iname "*.json" -type f -exec run_fia_cli.sh -u -d -f {} \; &>> out_bulk_upload_redo_1000_4.psv

# running all remaining and few redos
find ./data/redo_2000_5/ -iname "*.json" -type f -exec run_fia_cli.sh -u -d -f {} \; &>> out_bulk_upload_redo_2000_5.psv



```

### done ###

```
cat ./data/start/laravel_laravel_8.6.10_cdx_1.3_sbom.json | grep "content" | grep -oP '[a-f0-9]{40}'
cat ./data/log4j/log4j-web_log4j_2.15.0_cyclonedx_1.3_sbom.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"'

cat ./data/log4j/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | wc
cat ./data/start/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | wc

cat ./data/start/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | sort | uniq | wc
cat ./data/log4j/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | sort | uniq | wc

cat ./data/redo_500/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | wc && cat ./data/redo_500/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | sort | uniq | wc
cat ./data/redo_1000/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | wc && cat ./data/redo_1000/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | sort | uniq | wc
cat ./data/redo/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | wc && cat ./data/redo/*.json | grep "content" | grep -oP '\"[a-f0-9]{40}\"' | sort | uniq | wc
```

* these are shared to Matthew N. for manual upload via UI
```
cp "./data/start/python_python_3.10.6_cdx_1.4_sbom.json"  ~/share_host/tmp/
cp "./data/start/apache_tomcat_9.0.63_cdx_1.4.json"  ~/share_host/tmp/
cp "./data/start/log4j_2.17.2_cyclonedx_1.3_sbom.json"  ~/share_host/tmp/
```

* manual upload via UI done, redo these via cli
```
run_fia_cli.sh -d -b ./five_vendor_product.csv &>> out_bulk_create_vendor_product.log

run_fia_cli.sh -u -d -f "./data/start/python_python_3.10.6_cdx_1.4_sbom.json" &>> out_bulk_upload_start.psv

# fail due to missing Vendor # run_fia_cli.sh -u -d -f "./data/start/apache_tomcat_9.0.63_cdx_1.4.json" &>> out_bulk_upload_start.psv
cat ~/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss/data/start/apache_tomcat_9.0.63_cdx_1.4.json | ./fia register-sbom -api -vendor 'Open Source' -product 'Tomcat' -type 'Open Source' -filename 'apache_tomcat_9.0.63_cdx_1.4.json'


# upload few to start -u use_sbom_product json.metadata.component.name and use that with default vendor
run_fia_cli.sh -u -d -f "./data/start/wang-bin_QtAV_v1.12.0_sbom.json" &>> out_bulk_upload_start.psv
run_fia_cli.sh -u -d -f "./data/start/zeux_pugixml_v1.12.1_sbom.json" &>> out_bulk_upload_start.psv
run_fia_cli.sh -u -d -f "./data/start/meetfranz_franz_v5.9.2_sbom.json" &>> out_bulk_upload_start.psv
```

* do -b bulk create start - done
```
run_fia_cli.sh -d -b ./start_vendor_product.csv &>> out_bulk_create_vendor_product.log
```

* do -b bulk create of products under Open Source, has dupe - done
```
run_fia_cli.sh -d -b ./redo_vendor_product.csv &>> out_bulk_create_vendor_product.log

# if fail
run_fia_cli.sh -d -b ./part_aa_vendor_product.csv &>> out_bulk_create_vendor_product_aa.log
run_fia_cli.sh -d -b ./part_ab_vendor_product.csv &>> out_bulk_create_vendor_product_ab.log
run_fia_cli.sh -d -b ./part_ac_vendor_product.csv &>> out_bulk_create_vendor_product_ac.log
run_fia_cli.sh -d -b ./part_ad_vendor_product.csv &>> out_bulk_create_vendor_product_ad.log
```


* go rest of start (4181 sha-1 hashes 767 uniq) - done
```
find ./data/start/ -iname "*.json" -type f -exec run_fia_cli.sh -u -d -f {} \; &>> out_bulk_upload_start.psv
```

* go log4j (4181 sha-1 hashes 1285 uniq) - done
* 150 attempts, 4 fail
    * log4j_2.17.2_cyclonedx_1.3_sbom.json
    * log4j_2.3.2_cyclonedx_1.3_sbom.json
    * log4j_2.15.0_cyclonedx_1.3_sbom.json
    * log4j_2.12.4_cyclonedx_1.3_sbom.json
```
find ./data/log4j/ -iname "*.json" -type f -exec run_fia_cli.sh -u -d -f {} \; &>> out_bulk_upload_log4j.psv
```


* upload 500 (90493 sha-1 hashes 12943 uniq ) - done working
    * success 332
    * 
```
find ./data/redo_500/ -iname "*.json" -type f -exec run_fia_cli.sh -u -d  -f {} \; &>> out_bulk_upload_500.psv
```


* upload 1000 (170826 sha-1 hashes 18111 uniq) 
    * Starting +1000 upload, ETA 5:20 pm est
```
find ./data/redo_1000/ -iname "*.json" -type f -exec run_fia_cli.sh -u -d  -f {} \; &>> out_bulk_upload_1000.psv
```


* upload log4j stats
```
./parse_upload.sh -f ./data/log4j/out_bulk_upload_log4j.psv 
File: /home/fis/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss/data/log4j/out_bulk_upload_log4j.psv
Processed SBoMs count: 151
Success: 146
Missing or Invalid root component version, or duplicate: 4
No API response: 0

=============== No API response SBoMs ====================================


=============== Error (Must specify path or product) SBoMs =================
log4j_2.12.4_cyclonedx_1.3_sbom.json
log4j_2.3.2_cyclonedx_1.3_sbom.json
log4j_2.15.0_cyclonedx_1.3_sbom.json

=============== Error (Duplicate SBoM) ====================================
log4j_2.17.2_cyclonedx_1.3_sbom.json

```

* Upload start stats
```
./parse_upload.sh -f ./data/start/out_bulk_upload_start.psv 
File: /home/fis/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss/data/start/out_bulk_upload_start.psv
Processed SBoMs count: 20
Success: 19
Missing or Invalid root component version: 1
No API response: 0

=============== No API response SBoMs ====================================

=============== Error (Must specify path or product) SBoMs =================
apache_tomcat_9.0.63_cdx_1.4.json

```

* Upload 500 stats
```
->$ ./parse_upload.sh -f ./data/redo_500/out_bulk_upload_500.psv
File: /home/fis/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss/data/redo_500/out_bulk_upload_500.psv
Processed SBoMs count: 500
Success: 332
Missing or Invalid root component version, or Must specify patch or product: 159
No API response: 9

=============== No API response SBoMs ====================================
cloudera_livy_sbom.json
fyne-io_fyne_v2.1.4_sbom.json
kizniche_Mycodo_v8.13.9_sbom.json
barryvdh_laravel-ide-helper_v2.12.3_sbom.json
naiba_nezha_sbom.json
jdkato_prose_v1.2.1_sbom.json
pcwalton_sprocketnes_sbom.json
F5OEO_rpitx_v1_sbom.json
Shougo_deoplete.nvim_5.2_sbom.json

=============== Error (Must specify patch or product) SBoMs =================
libffi_libffi_v3.4.2_sbom.json
flatpak_flatpak_1.12.7_sbom.json
bitwarden_browser_sbom.json
golang_protobuf_sbom.json
neon-bindings_neon_sbom.json
```

* upload 1000 stats
```
parse_upload.sh -f ./data/redo_1000/out_bulk_upload_1000.psv 
File: /home/fis/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss/data/redo_1000/out_bulk_upload_1000.psv
Processed SBoMs count: 1000
Success: 675
Missing or Invalid root component version, or Must specify patch or product: 325
No API response: 0

=============== No API response SBoMs ====================================


=============== Error (Must specify patch or product) SBoMs =================
qpdf_qpdf_release-qpdf-10.6.3_sbom.json
uuid-rs_uuid_sbom.json
go-sql-driver_mysql_v1.6.0_sbom.json
cockpit-project_cockpit_270_sbom.json
uutils_coreutils_sbom.json
chrome-php_chrome_v1.6.0_sbom.json
flannel-io_flannel_v0.17.0_sbom.json
fwupd_fwupd_1.8.0_sbom.json
google_cadvisor_v0.39.3_sbom.json
prometheus_pushgateway_v1.4.2_sbom.json
LANL-Bioinformatics_EDGE_v2.4.0_sbom.json
BurntSushi_toml_v1.1.0_sbom.json
keybase_client_v5.9.3_sbom.json
symfony_filesystem_sbom.json
gogo_protobuf_v1.3.2_sbom.json
harfbuzz_harfbuzz_4.3.0_sbom.json
```