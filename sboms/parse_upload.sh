#!/bin/bash

# Point to folder containing sboms, output to sdtout

vendor='Open Source'

while getopts f: flag
do
    case "${flag}" in
        f) outfile=${OPTARG};;
    esac
done

outfile=$(realpath "${outfile}")
txt=$(cat "${outfile}")

# ./parse_upload.sh -f ./data/redo_500/out_bulk_upload_500.psv 

# "error": "must specify patch or product"

echo "File: ${outfile}"

processed=$(echo "$txt" | grep '|Processing' | wc -l)
echo "Processed SBoMs count: $processed"

success=$(echo "$txt" | grep '"isSuccess": true' | wc -l)
echo "Success: $success"

success_warnings=$(echo "$txt" | grep 'Success_warnings' | wc -l)
#echo "Success with expected Warnings: $success_warnings"

fail=$(echo "$txt" | grep '"isSuccess": false' | wc -l)
echo "Missing or Invalid root component version: $fail"

version_missing=$(echo "$txt" | grep -B 1 '"isSuccess": false' | grep '"error"' | grep 'missing: component version.' | wc -l)
# echo "Error (Missing component version): $version_missing"

no_response=$(echo "$txt" | grep 'No_isSuccess' | wc -l )
echo "No API response: $no_response"

#echo "$txt" | grep 'No_isSuccess' | cut -d '|' -f 3
echo ''
echo '=============== No API response SBoMs ===================================='
basename -a "$(echo "$txt" | grep 'No_isSuccess' | cut -d '|' -f 3)"

echo ''
echo '=============== Error (Must specify path or product) SBoMs ================='
basename -a "$(echo "$txt" | grep -B 1 '"must specify patch or product"' | grep 'DEBUG' | cut -d '|' -f 3)"
echo ''

exit 0
