#!/bin/bash

# Point to folder containing sboms, output to sdtout

vendor='Open Source'

while getopts f: flag
do
    case "${flag}" in
        f) folder=${OPTARG};;
    esac
done
product=''
json=''
folder=$(realpath "${folder}")
sboms=$(find "${folder}" -type f -iname "*.json")

echo 'vendor,product'
for file in $sboms;do
    json=$(cat "${file}" | jq '.')
    product=$(echo "$json" | jq -r '.metadata.component.name')
    echo "$vendor,$product"
done
