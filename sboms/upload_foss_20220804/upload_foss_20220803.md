# upload FOSS #

## commands ##
```
cd ~/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss
find ./redo_upload_foss_20220719/ -type f -exec run_fia_cli.sh -d true -f {} \; >> test_20220803T1545.psv

# then
find ../FOSS_SBoMs/ -type f -exec run_fia_cli.sh -d true -f {} \; >> test_20220803T??.psv

```

done setup vendor 'Open Source'
done setup vendor product 'Various'

Fia daily standup , can we set the product name to the sbom.metadata.component.name ? no, needs to be legal identity

## stats 20220803 ##

* sboms upload attempted = 44
    * sboms with invalid login error = 27
        * sboms with invalid login not in system = 1 (andrewngu_sound-redux_sbom.json) due to no root component version
        * sboms upload success (even with invalid login error) = 26
            * confirms now accepting root component version with "v" ("v0.0.0")
    * sboms fail cause no root version = 14
    * sboms fail cause duplicate = 3
* remaining redo_upload_foss_20220719 = 668
* remaining FOSS_SBoMs = 5775
* now accepting root component version with v character "v0.0.0" are accepted

* sboms to redo = 712
    * 704 with updated timestamps, + 9 with updated timestamps and various edits
* sboms to do = 5775



## run ##

* output file = test_20220803T1530.psv
* sboms that got invalid login and success load
    * single sbom not in system - andrewngu_sound-redux_sbom.json
        * no root component version
```
alievk_avatarify-python_v0.2.0_sbom.json
ampache_ampache_sbom.json 

cvxpy_cvxpy_v1.2.1_sbom.json
docker_build-push-action_sbom.json
dsherret_ts-morph_sbom.json
fastify_fast-json-stringify_v3.2.0_sbom.json
FRRouting_frr_frr-8.2.2_sbom.json
iBaa_PlexConnect_sbom.json
Icinga_icinga2_v2.12.8_sbom.json
kahing_goofys_v0.24.0_sbom.json
KevinMusgrave_pytorch-metric-learning_v1.3.0_sbom.json
laravel_cashier-stripe_sbom.json
laravel-filament_filament_v2.12.10_sbom.json
leecade_react-native-swiper_v1.6.0_sbom.json
localstack_localstack_v0.14.2_sbom.json
lucas-clemente_quic-go_v0.27.0_sbom.json
mackwic_colored_sbom.json
michaelb_sniprun_sbom.json
petkaantonov_bluebird_v3.7.2_sbom.json
pixie-io_pixie_sbom.json
rubysec_bundler-audit_report_sbom.json
shapely_shapely_1.8.2_sbom.json
symfony_security-bundle_v6.0.8_sbom.json
taskflow_taskflow_sbom.json
Tencent_vConsole_sbom.json
tlaverdure_laravel-echo-server_sbom.json
```

* run
```
# second session 8fdbb3b51cfc50837de4baff6cd97a2fe8607515f7e21ba1d48b820aa6eb6a04fbcc01ffabba6d5f192e0f4a8fe9c45d
find ./redo_upload_foss_20220719/ -type f -exec run_fia_cli.sh -d true -f {} \; >> test_20220803T1545.psv

{
  "isSuccess": false,
  "message": "Invalid login: 535 5.7.139 Authentication unsuccessful, the request did not meet the criteria to be authenticated successfully. Contact your administrator. [BL0PR03CA0015.namprd03.prod.outlook.com]"
}
```

next session = d07fd91b13144fc09ac001087652c0ef93e2babb7e62ece5c0fa3172b973a03eece57668dd115c3fa96945c944df9793
```
run_fia_cli.sh -l true 
realpath: '': No such file or directory
[INFO]|2022-08-03T20:56:44Z||Login dlambert@fortressinfosec.com
[INFO   ]       2022/08/03 20:56:44 login initiated
password: 
[DEBUG  ]       2022/08/03 20:56:53 [FIAAPI.Login] - POST: https://fia-dev.cybrek.com/login/local/auth
[DEBUG  ]       2022/08/03 20:56:54 [FIAAPI.Login] - status: 200
[INFO   ]       2022/08/03 20:56:54 authenticated with token d07fd91b13...c944df9793
[INFO]|2022-08-03T20:56:44Z||Fin
```

* next run
```
find ./unk_invalid_login/ -iname "*.json" -exec run_fia_cli.sh -d true -f {} \; >> test_20220803T1700.psv
find ./redo_upload_foss_20220719/ -iname "*.json" -exec run_fia_cli.sh -d true -f {} \; >> test_20220803T1730.psv
```


## Change stuff ##

* various changes list, filenames explain, (~/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss/adjusted_sboms)
    * sbom test single - mimikatz_sha256_20180710155616-bc664df96737_0xDkd_auxpi_2.3.9_sbom.json
        * second version uploaded 20220803
        * expect malware mimikatz to hit on
            * "pkg:golang/github.com/bradfitz/gomemcache@v0.0.0-20180710155616-bc664df96737"
            * and due to above additional hit in dependsOn
                * "pkg:golang/github.com/astaxie/beego@v1.11.1"
    * sbom - rootKit_sha256_20180516100307-2d684516a886_wercker_stern_sbom.json
        * expect malware rootkit hit on 
            * "pkg:golang/github.com/fatih/color@v1.7.1-0.20180516100307-2d684516a886"
* File names below indicate change in sbom since last upload.
```
add_comp_MarkBaker_PHPMatrix_sbom.json
add_dependsOn_yiisoft_yii2_sbom.json
add_lic_root_comp_brick_math_sbom.json
chg_comp_ver_uuid-generator_broadway_broadway_2.4.0_sbom.json
lic_chg_cyclonedx-library_simplepie_simplepie_1.6.0_sbom.json
loss_comp_ampache_ampache_sbom.json
loss_comp_depen_ssl_ezXSS_sbom.json
rootKit_sha256_20180516100307-2d684516a886_wercker_stern_sbom.json
```


*  all uploads done now have adjusted timestamps and sbom version

### notes ###

* previously done and has dependsOn 
```
 grep 'dependsOn' ../upload_foss_20220719/done/* | sort | uniq
../upload_foss_20220719/done/acacha_adminlte-laravel_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/ampache_ampache_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/ampproject_amp-wp_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/banago_PHPloy_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/blade-ui-kit_blade-icons_1.3.0_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/brick_math_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/broadway_broadway_2.4.0_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/causefx_Organizr_1.90_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/cmgmyr_laravel-messenger_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/CodisLabs_codis_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/doctrine_dbal_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/Dolibarr_dolibarr_12.0.5_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/flatpak_flatpak_1.12.7_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/gabrielrcouto_php-terminal-gameboy-emulator_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/istio_istio_1.12.7_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/JohnTroony_php-webshells_1.1_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/klein0r_fhem-docker_0.1_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/lonnieezell_Bonfire_0.7.8_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/MarkBaker_PHPMatrix_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/mautic_mautic_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/monitoror_monitoror_4.0.1_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/mpociot_laravel-apidoc-generator_4.8.2_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/phan_phan_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/phar-io_manifest_2.0.3_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/phpDocumentor_ReflectionDocBlock_5.3.0_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/php-pm_php-pm_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/phpseclib_phpseclib_3.0.14_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/roundcube_roundcubemail_1.5.2_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/schmittjoh_metadata_2.6.1_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/schmittjoh_php-option_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/seblucas_cops_1.1.3_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/simplepie_simplepie_1.6.0_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/spatie_crawler_7.1.1_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/spatie_image-optimizer_1.6.2_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/spatie_laravel-translatable_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/spatie_period_2.3.3_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/ssl_ezXSS_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/SwooleDistributed_SwooleDistributed_3.7.4_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/tenancy_multi-tenant_5.7.3_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/thephpleague_glide_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/thephpleague_oauth2-server_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/Tucker-Eric_EloquentFilter_3.1.0_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/tymondesigns_jwt-auth_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/vimeo_psalm_4.23.0_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/wercker_stern_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/yiisoft_yii_1.1.25_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/yiisoft_yii2_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/zephir-lang_zephir_0.16.0_sbom.json:      "dependsOn": [
../upload_foss_20220719/done/zircote_swagger-php_4.4.2_sbom.json:      "dependsOn": [
```