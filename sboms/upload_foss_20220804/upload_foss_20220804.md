# upload FOSS #

## commands ##
```
cd ~/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss
# session 48ad22d931900fe7b67d60123294eb9368f937ab772308d18856602265153b9e77b03c81d8a06955cea65739130d46b7
# test script = good
run_fia_cli.sh -d true -f ./redo_upload_foss_20220719/abouolia_sticky-sidebar_3.2.0_sbom.json > ./out_files/test_abouolia_sticky-sidebar_3.2.0_sbom.psv

# run !!! start 1307 to 1339 - done
find ./redo_upload_foss_20220719/ -type f -iname "*.json" -exec run_fia_cli.sh -f {} \; >> ./out_files/test_20220804T1300.psv

# remaining
date && find ../FOSS_SBoMs/ -type f -iname "*.json" -exec run_fia_cli.sh -f {} \; >> ./out_files/test_20220804T1300.psv && date

find ./fail/fail_no_root_version/ -iname "*.json" -exec jq -r '.metadata.component.version' {} \; | sort | uniq

```

## stats 20220804 ##

### Summary ###

Stats are overall for Aug 3, 2022 and Aug 4, 2022.

Upload into FIA-DEV under: vendor 'Open Source', product 'Various'
* Total SBoMs Processed/Attempted = 6443 
    * Upload rate = 1140 SBoMs/hr
* Total SBoMs Success Upload = 4361
* Total SBoMs Not Accepted = 2126
    * Not Accepted Reasons below.
    * Missing/Empty Root Component version = 2046 (96% of Total SBoMs Not Accepted)
    * Missing Root Component supplier, author, publisher, or group = 13 
    * Duplicate SBoMs = 27
    * Invalid Login = 26 (Issue fixed in the morning on second day)
    * API not return isSuccess = 13 (under review)
    * Unknown Reason = 1 (under review)

### Notes ###

* test sbom single = 1
* redo sbom count = 667 out of 6443
    * processing time = 667 sboms/32min = 1252 sboms/hr
* remaining sbom count = 5775
* time to upload 5hr 23 min = 323 min = 19 sboms/ min = 1140 sboms / hr
    * start 2022-08-04T17:06:22Z
    * end 2022-08-04T22:29:42Z

```
find . -maxdepth 1 -mindepth 1 -type d -exec sh -c 'echo "{} : $(find "{}" -type f | wc -l)" file\(s\)' \;
./redo_upload_foss_20220719 : 0 file(s)
./out_files : 5 file(s)
./done : 4361 file(s)
./mimi_test : 16 file(s)
./fail : 2126 file(s)
./adjusted_sboms : 9 file(s)

find ./fail/ -maxdepth -maxdepth 1 -mindepth 1 -type d -exec sh -c 'echo "{} : $(find "{}" -type f | wc -l)" file\(s\)' \;
./fail/invalid_login : 26 file(s)
./fail/no_isSuccess : 13 file(s)
./fail/fail_no_root_version : 2046 file(s)
./fail/no_responce : 0 file(s)
./fail/fail_unk : 1 file(s)
./fail/bad_group : 13 file(s)
./fail/fail_dupe : 27 file(s)
./fail/fail_values : 0 file(s)
./fail/bad_sbom_json : 0 file(s)
```


## Change stuff from last upload ##

* various changes list, filenames explain, (~/ws/InvokeGithubExtractor/invoke_github_extractor/sboms/upload_foss/adjusted_sboms)
    * sbom test single - mimikatz_sha256_20180710155616-bc664df96737_0xDkd_auxpi_2.3.9_sbom.json
        * second version uploaded 20220803
        * expect malware mimikatz to hit on
            * "pkg:golang/github.com/bradfitz/gomemcache@v0.0.0-20180710155616-bc664df96737"
            * and due to above additional hit in dependsOn
                * "pkg:golang/github.com/astaxie/beego@v1.11.1"
    * sbom - rootKit_sha256_20180516100307-2d684516a886_wercker_stern_sbom.json
        * expect malware rootkit hit on 
            * "pkg:golang/github.com/fatih/color@v1.7.1-0.20180516100307-2d684516a886"
* File names below indicate change in sbom since last upload.
```
add_comp_MarkBaker_PHPMatrix_sbom.json
add_dependsOn_yiisoft_yii2_sbom.json
add_lic_root_comp_brick_math_sbom.json
chg_comp_ver_uuid-generator_broadway_broadway_2.4.0_sbom.json
lic_chg_cyclonedx-library_simplepie_simplepie_1.6.0_sbom.json
loss_comp_ampache_ampache_sbom.json
loss_comp_depen_ssl_ezXSS_sbom.json
rootKit_sha256_20180516100307-2d684516a886_wercker_stern_sbom.json
```


*  all uploads done now have adjusted timestamps and sbom version

## Post Upload Review ##

* bad_group bucket should have had group, need fix create_sbom.sh
* 

