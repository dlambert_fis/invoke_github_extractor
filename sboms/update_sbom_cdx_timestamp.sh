#!/bin/bash
# find ./redo_upload_foss_20220719/ -iname "*.json" -exec ./update_sbom_cdx.sh -f {} \;
# find ./upload_foss/redo_product/ -iname "*.json" -exec ./update_sbom_cdx.sh -f {} \;

now="$(date -I'seconds' -u | sed 's/\+00\:00/Z/g')"
while getopts f: flag
do
    case "${flag}" in
        f) file=${OPTARG};;
       
    esac
done

file=$(realpath ${file})
json=$(cat ${file} | jq '.')
version=$(echo "$json" | jq -r '.version')
version=$((version+1))
json=$(echo "$json" | jq ".version = $version" | jq ".metadata.timestamp = \"$now\"")
echo "$json" > ${file}
sleep 0.1
exit 0
#echo "$json" | jq ".metadata.timestamp = \"$now\""
