#!/bin/bash

# find ./FOSS_SBoMs/ -iname "*.json" -exec ./

while getopts f:t: flag
do
    case "${flag}" in
        f) file=${OPTARG};; # point to cyclonedx sbom in json format
        t) token=${OPTARG};; # github access token
    esac
done


file=$(realpath ${file})
file_name=$(basename ${file})

json=$(cat "$file" | jq '.')
if [[ $? != 0 ]];then
    echo "Fail reading JSON file returncode $?:${file}"
    exit $?
fi

owner=$(echo $json | jq -r '.metadata.component.group')
project=$(echo $json | jq -r '.metadata.component.name')
version=$(echo $json | jq -r '.metadata.component.version')

# echo "sbom_file,vendor,project,version" > FOSS_SBoMs_projects.csv
echo "$file_name,$owner,$project,$version"
exit 0

repos_url='https://api.github.com/repos'
header_json='Accept: application/vnd.github.v3+json'
header_auth="Authorization: token $token"
gh_apiurl="$repos_url/${owner}/${project}"
gh_url="https://github.com/${owner}/${project}"
homepage='null'

# test api, owner website is githubapi.homepage
res=$(curl -s -H "$header_json" -H "$header_auth" "$gh_apiurl")
curl_ret=$?
if [[ $curl_ret != 0 ]];then
    echo "$owner,$project,null,$gh_apiurl"
    echo "Fail Github API call ret:$curl_ret, URL:$gh_apiurl, SBOM:$file"
    exit $curl_ret
fi

has_homepage=$(echo "$res" | jq '. | has("homepage")'| grep true | head -1)
if [[ $has_homepage == true ]];then
    homepage=$(echo "$res" | jq -r '.homepage')
    if [[ $homepage == '' ]];then #consitant null instead of empty string
        homepage='null'
    fi
fi 

website_tag="{ \"type\":\"website\", \"url\":\"${homepage}\", \"comment\":\"As set via \`homepage\` in Github API\" }"

# update sbom
has_sbom_website=$(echo "$json" | jq ".metadata.component.externalReferences[].url | contains(\"${homepage}\")" | grep true | head -1)
if [[ $has_sbom_website != true ]];then
    json=$(echo "$json" | jq ".metadata.component.externalReferences[.metadata.component.externalReferences|length] += ${website_tag}")
    jq_ret=$?
fi

# update file
if [[ $jq_ret == 0 ]];then
    echo "$json" > "$file"
fi

# owner,project,website
echo "$owner,$project,$homepage,$gh_apiurl"

sleep 1.1
exit 0

''' code notes

json=$(echo "$json" | jq ".metadata.authors[.metadata.authors|length] += ${name_tag}")
contains_fortress_author=$(echo $json | jq ".metadata.authors[].name | contains("Fortress Information Security")" | grep true | head -1)

curl \
  -H "Accept: application/vnd.github.v3+json" \ 
  -H "Authorization: token <TOKEN>" \
  https://api.github.com/repos/OWNER/REPO


giturl="${gh_apiurl}.git"
exter_ref="{\"url\":\"${giturl}\",\"type\":\"distribution\"}"
exter_ref_array="{\"externalReferences\":[${exter_ref}]}"
has_exter_ref_array=$(echo ${json} | jq '.metadata.component | has("externalReferences")')
if [[ ${has_exter_ref_array} == false ]];then
    json=$(echo "${json}" | jq ".metadata.component += ${exter_ref_array}")
fi

if [[ ${has_exter_ref_array} == true ]];then
    has_exter_ref=$(echo ${json} | jq ".metadata.component.externalReferences[].url | contains(\"${giturl}\")" | grep "true" | head -1)
    if [[ ${has_exter_ref} != true ]];then
        json=$(echo "${json}" | jq ".metadata.component.externalReferences[.metadata.component.externalReferences|length] += ${exter_ref}")
    fi
    
fi
'''


